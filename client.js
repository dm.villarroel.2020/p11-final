// client.js

const configuration = { iceServers: [] };
let pc = new RTCPeerConnection(configuration);

const useStunCheckbox = document.getElementById('use-stun');
const startButton = document.getElementById('start');
const stopButton = document.getElementById('stop');
const audioElement = document.getElementById('audio');
const videoElement = document.getElementById('video');

let localStream;

async function start() {
    try {
        localStream = await navigator.mediaDevices.getUserMedia({ audio: true, video: true });
        localStream.getTracks().forEach(track => pc.addTrack(track, localStream));

        audioElement.srcObject = localStream;
        videoElement.srcObject = localStream;

        startButton.style.display = 'none';
        stopButton.style.display = 'inline-block';

        // Your signaling logic goes here (e.g., send offer to the signaling server)
    } catch (error) {
        console.error('Error accessing media devices:', error);
    }
}

function stop() {
    if (localStream) {
        localStream.getTracks().forEach(track => track.stop());
        audioElement.srcObject = null;
        videoElement.srcObject = null;

        startButton.style.display = 'inline-block';
        stopButton.style.display = 'none';

        // Your signaling logic goes here (e.g., send stop signal to the signaling server)
    }
}

useStunCheckbox.addEventListener('change', () => {
    // Your logic for handling STUN checkbox change
    // You may need to update your RTCPeerConnection configuration based on the checkbox state
});

// Add event listeners for RTCPeerConnection events (e.g., onicecandidate, ontrack) as needed
