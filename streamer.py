import asyncio
import json
import websockets
import sys
import subprocess
from aiortc import RTCPeerConnection, RTCSessionDescription

import base64

file_name = sys.argv[1]
signal_ip = sys.argv[2]
signal_port = int(sys.argv[3])

from aiortc.contrib.media import MediaStreamTrack

class VideoStreamTrack(MediaStreamTrack):
    kind = 'video'
    def __init__(self, file_name):
        super().__init__()
        self.process = subprocess.Popen(
            [
                "ffmpeg",
                "-i", file_name,
                "-f", "image2pipe",
                "-pix_fmt", "yuv420p",  # Cambiar a yuv420p
                "-vcodec", "rawvideo",
                "-",
            ],
    stdout=subprocess.PIPE,
    stderr=subprocess.PIPE,
    bufsize=10**8,  # Este es el argumento bufsize
        )

    async def recv(self):
        raw_image = self.process.stdout.read(640 * 480 * 3)
        print(len(raw_image))
        if not raw_image:
            return None
        pts, time_base = self.generate_timestamp()
        return raw_image, pts, time_base

    def generate_timestamp(self):
        return self.process.stdout.tell(), 90000  # 90 kHz time base

async def send_offer(pc, signal_ip, signal_port, file_name):
    try:
        offer = await pc.createOffer()
        await pc.setLocalDescription(offer)

        offer_sdp = pc.localDescription.sdp
        offer_sdp_base64 = base64.b64encode(offer_sdp.encode("utf-8")).decode("utf-8")

        message = {
            "type": "offer",
            "sdp": offer_sdp_base64,
            "file_name": file_name,
        }

        uri = f"ws://{signal_ip}:{signal_port}"
        async with websockets.connect(uri) as ws:
            print(f"Sending offer to {uri}")
            await ws.send(json.dumps(message))

            response = await ws.recv()
            response_data = json.loads(response)

            if response_data["type"] == "answer":
                answer_sdp_base64 = response_data["sdp"]
                answer_sdp = base64.b64decode(answer_sdp_base64).decode("utf-8")

                answer = RTCSessionDescription(sdp=answer_sdp, type="answer")
                await pc.setRemoteDescription(answer)

                print("WebRTC communication established.")
            else:
                print("Unexpected response from signaling server.")

    except Exception as e:
        print(f"Error in send_offer: {e}")


async def stream_video(file_name, signal_ip, signal_port):
    pc = RTCPeerConnection()

    @pc.on("datachannel")
    def on_datachannel(channel):
        print("Data channel opened")

    video_track = VideoStreamTrack(file_name)
    pc.addTrack(video_track)

    await send_offer(pc, signal_ip, signal_port, file_name)

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python stream.py <file> <signal_ip> <signal_port>")
        sys.exit(1)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(stream_video(file_name, signal_ip, signal_port))

