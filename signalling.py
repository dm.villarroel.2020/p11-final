import asyncio
import sys
import socket

class SignalingServer:
    def __init__(self, signal_port):
        self.signal_port = signal_port
        self.streamers = {}  # Diccionario para almacenar información de streamers

    async def handle_message(self, message, addr):
        # Manejar los diferentes tipos de mensajes UDP

        # Registro de streamer
        if message.startswith("Registro de streamer"):
            # Analizar el mensaje para obtener información del streamer
            parts = message.split(",")
            if len(parts) >= 2:
                file_name = parts[1].strip()
                self.streamers[file_name] = addr  # Almacenar información del streamer
                print(f"Se registró el streamer {file_name} desde {addr[0]}:{addr[1]}")

        # Petición de lista de ficheros
        elif message.startswith("Petición de lista de ficheros"):
            # Enviar la lista actualizada de archivos registrados al servidor frontal
            file_list = ",".join(self.streamers.keys())
            self.send_message("Respuesta lista de ficheros:" + file_list, addr)

        # Oferta de navegador
        elif message.startswith("Oferta de navegador"):
            parts = message.split(",")
            if len(parts) >= 3:
                file_name = parts[1].strip()
                if file_name in self.streamers:
                    streamer_addr = self.streamers[file_name]
                    offer_message = ",".join(parts[2:])  # Obtener la oferta SDP del mensaje
                    print(f"Recepción de oferta: Oferta para {file_name} desde {addr[0]}:{addr[1]}")

                    # Aquí se enviaría la oferta al streamer correspondiente usando send_message()
                    self.send_message(f"Mensaje de oferta:{offer_message}", streamer_addr)
                    print(f"Envío de oferta: Oferta para {file_name} enviada a {streamer_addr[0]}:{streamer_addr[1]}")


        # Respuesta de streamer
        elif message.startswith("Respuesta de streamer"):
            # Enviar la respuesta SDP del streamer al servidor frontal
            parts = message.split(",")
            if len(parts) >= 3:
                browser_addr = (parts[1].strip(), int(parts[2].strip()))
                response_message = ",".join(parts[3:])  # Obtener la respuesta SDP del mensaje
                print(f"Recepción de respuesta: Respuesta desde {addr[0]}:{addr[1]}")

                # Aquí se enviaría la respuesta al servidor frontal usando send_message()
                self.send_message(f"Mensaje de respuesta:{response_message}", browser_addr)
                print(
                    f"Envío de respuesta: Respuesta enviada al servidor frontal en {browser_addr[0]}:{browser_addr[1]}")

    async def start_server(self):

        # Iniciar el servidor UDP
        transport, protocol = await asyncio.get_event_loop().create_datagram_endpoint(
            lambda: DatagramProtocol(self.handle_message),
            local_addr=('0.0.0.0', self.signal_port)
        )
        print("Comienzo: Iniciando el servidor de señalización...")
        print(f"Servidor de señalización iniciado en el puerto {self.signal_port}")

        # Obtener la dirección IP del servidor
        host_ip = socket.gethostbyname(socket.gethostname())
        print(f"La dirección IP del servidor de señalización es: {host_ip}")
        try:
            while True:
                await asyncio.sleep(1)  # Mantener el bucle de eventos activo
        except KeyboardInterrupt:
            print("Se interrumpió la señal. Cerrando el servidor...")
        finally:
            transport.close()  # Cerrar la conexión del servidor

    def send_message(self, message, addr):
        # Método para enviar mensajes UDP
        # Aquí se implementaría el envío de mensajes a través de sockets UDP
        pass


class DatagramProtocol:
    def __init__(self, message_handler):
        self.message_handler = message_handler

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        asyncio.ensure_future(self.message_handler(message, addr))


if __name__ == "__main__":
    signal_port = int(sys.argv[1])  # Puerto para el servidor de señalización
    signaling_server = SignalingServer(signal_port)
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(signaling_server.start_server())
    except KeyboardInterrupt:
        pass
    finally:
        loop.close()