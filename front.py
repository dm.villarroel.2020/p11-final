import argparse
import asyncio
import json
import logging
import os
import platform
import ssl

from aiortc.contrib.signaling import CopyAndPasteSignaling
from aiohttp import web
from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.media import MediaPlayer, MediaRelay

ROOT = os.path.dirname(__file__)

async def index(request):
    content = open(os.path.join(ROOT, "index.html"), "r").read()
    return web.Response(content_type="text/html", text=content)


async def javascript(request):
    content = open(os.path.join(ROOT, "client.js"), "r").read()
    return web.Response(content_type="application/javascript", text=content)


async def offer(request):
    signaling = CopyAndPasteSignaling()

    await signaling.connect()
    params = await request.json()
    offer_ = RTCSessionDescription(sdp=params["sdp"], type=params["type"])
    await signaling.send(offer_)

    while True:
        obj = await signaling.receive()
        if isinstance(obj, RTCSessionDescription):
            return web.Response(
                content_type="application/json",
                text=json.dumps(
                    {"sdp": obj.sdp, "type": obj.type}
                ),
            )


pcs = set()
async def on_shutdown(app):
    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)
    pcs.clear()

# ######################## Main ###############################################

def main():

    parser = argparse.ArgumentParser(description="Front Server")
    parser.add_argument("http_port", type=int, help="HTTP port for the front server")
    parser.add_argument("signal_ip", type=str, help="Signal server IP")
    parser.add_argument("signal_port", type=int, help="Signal server port")
    args = parser.parse_args()

    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get("/", index)
    app.router.add_get("/client.js", javascript)
    app.router.add_post("/offer", offer)
    web.run_app(app, host="localhost", port=args.http_port)

if __name__ == "__main__":
    main()
